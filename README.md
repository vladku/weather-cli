# Weather CLI

CLI tool to get weather from different providers.

USAGE:
    weather \<SUBCOMMAND>

OPTIONS:

    -h, --help       Print help information
    -V, --version    Print version information

SUBCOMMANDS:

    configure    Configure credentials for \<provider>
    get          Get weather for \<address>
    help         Print this message or the help of the given subcommand(s)

> Use `weather help <SUBCOMMAND>` to see more details.

## Tests

Set env var with OpenWeather api key. Like:
`export OPEN_WEATHER_API_KEY=<your api key>`

> If you want to develop something please run `pre-commit install` before any commit
