#[macro_use]
extern crate serde_derive;

mod providers;

use clap::{Parser, Subcommand, ValueEnum};
use providers::graphql::GraphqlProvider;
use providers::open_weather::OpenWeatherProvider;

#[derive(Parser)]
#[clap(author, version)]
#[clap(propagate_version = true)]
#[clap(about = "CLI tool to get weather from different providers.", long_about = None)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}
#[derive(Subcommand)]
enum Commands {
    /// Configure credentials for <provider>
    Configure {
        /// Is short name for a concrete weather API
        #[clap(arg_enum, value_parser)]
        provider: Provider,
        /// API key for selected weather provider
        #[clap(arg_enum, value_parser, short, long)]
        api_key: Option<String>,
        /// Units type of temperature
        #[clap(arg_enum, value_parser, short, long)]
        units: Option<Units>,
    },
    /// Get weather for <address>
    Get {
        /// Name of city
        #[clap(value_parser)]
        address: String,
        /// Is short name for a concrete weather API
        #[clap(arg_enum, value_parser, short, long)]
        provider: Option<Provider>,
        /// Units type of temperature
        #[clap(arg_enum, value_parser, short, long)]
        units: Option<Units>,
    },
}
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Serialize, Deserialize)]
enum Provider {
    Graphql,
    OpenWeather,
}
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Serialize, Deserialize)]
enum Units {
    Metric,
    Imperial,
    Kelvin,
}
impl std::fmt::Display for Units {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Units::Metric => write!(f, "metric"),
            Units::Imperial => write!(f, "imperial"),
            Units::Kelvin => write!(f, "kelvin"),
        }
    }
}

#[derive(Debug)]
struct WeatherInfo {
    summary: String,
    temperature: f32,
    feels_like: f32,
    min: f32,
    max: f32,
    city: String,
    country: String,
    units: Units,
}
impl WeatherInfo {
    fn print(&self) {
        let units_symbol = match self.units {
            Units::Metric => "C",
            Units::Imperial => "F",
            Units::Kelvin => "K",
        };
        println!("{} in {} {}", self.summary, self.city, self.country);
        println!(
            "Temperature: {} {} (Feels Like: {} {})",
            self.temperature, units_symbol, self.feels_like, units_symbol
        );
        println!(
            "Min: {} {} Max: {} {}",
            self.min, units_symbol, self.max, units_symbol
        );
    }
}

trait WeatherProvider {
    fn get(&self, city: &str, units: Units) -> Result<WeatherInfo, String>;
}

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    default_provider: Provider,
    open_weather_api_key: Option<String>,
    units: Units,
}
impl ::std::default::Default for Config {
    fn default() -> Self {
        Self {
            open_weather_api_key: None,
            default_provider: Provider::Graphql,
            units: Units::Metric,
        }
    }
}

fn main() {
    let mut cfg = match confy::load_path("config") {
        Ok(c) => c,
        _ => Config {
            open_weather_api_key: None,
            default_provider: Provider::Graphql,
            units: Units::Metric,
        },
    };
    let cli = Cli::parse();
    match &cli.command {
        Commands::Configure {
            provider,
            api_key,
            units,
        } => {
            match provider {
                Provider::OpenWeather => cfg.open_weather_api_key = api_key.clone(),
                Provider::Graphql => (),
            };
            if let Some(units) = units {
                cfg.units = *units
            }
            cfg.default_provider = *provider;
            match confy::store_path("config", cfg) {
                Ok(_) => (),
                Err(e) => panic!("Failed to save config. {}", e),
            }
        }
        Commands::Get {
            address,
            provider,
            units,
        } => {
            let provider = match provider {
                Some(provider) => *provider,
                None => cfg.default_provider,
            };
            let provider: Box<dyn WeatherProvider> = match provider {
                Provider::Graphql => Box::new(GraphqlProvider{}),
                Provider::OpenWeather => match cfg.open_weather_api_key {
                    Some(api_key) => Box::new(OpenWeatherProvider::new(api_key)),
                    _ => panic!("Failed to use OpenWeather provider without api key. Please reconfigure app.")
                }
            };
            let units = match units {
                Some(units) => *units,
                None => cfg.units,
            };
            let info = match provider.get(address, units) {
                Ok(info) => info,
                Err(e) => panic!("Failed to get weather. {}", e),
            };
            info.print();
        }
    }
}

#[cfg(test)]
mod integration {
    use super::*;
    use std::env;

    #[test]
    fn open_weather() {
        let api_key = env::var("OPEN_WEATHER_API_KEY").unwrap();
        let provider = OpenWeatherProvider::new(api_key);
        let info = provider.get("lviv", Units::Metric).unwrap();
        assert_eq!(info.city, "Lviv");
    }
    #[test]
    fn graphql() {
        let provider = GraphqlProvider {};
        let info = provider.get("Kyiv", Units::Metric).unwrap();
        assert_eq!(info.city, "Kyiv");
    }
}
