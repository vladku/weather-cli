use crate::{Units, WeatherInfo, WeatherProvider};

pub struct GraphqlProvider;

impl WeatherProvider for GraphqlProvider {
    fn get(&self, city: &str, units: Units) -> Result<WeatherInfo, String> {
        let url = "https://graphql-weather-api.herokuapp.com/";
        let client = reqwest::blocking::Client::new();
        let query = gql(GetCityByNameQuery {
            name: city.to_string(),
            config: Config { units },
        });
        let info = match client
            .post(url)
            .header("Content-Type", "application/json")
            .body(query)
            .send()
        {
            Ok(r) => match r.json::<GQLResponse>() {
                Ok(r) => r.data.get_city_by_name,
                Err(e) => return Err(format!("{}", e)),
            },
            Err(e) => return Err(format!("{}", e)),
        };
        Ok(WeatherInfo {
            summary: info.weather.summary.title,
            temperature: info.weather.temperature.actual,
            feels_like: info.weather.temperature.feels_like,
            min: info.weather.temperature.min,
            max: info.weather.temperature.max,
            city: info.name,
            country: info.country,
            units,
        })
    }
}

fn gql(query: GetCityByNameQuery) -> String {
    let q = format!(
        "getCityByName(name: \\\"{}\\\", config: {{ units: {} }})",
        query.name, query.config.units
    );
    format!(
        "{{\"query\":\"{{{}{{name\\n country\\n weather {{summary {{title}}\\n temperature {{actual min max feelsLike}} }} }} }}\"}}",
        q
    )
}
struct GetCityByNameQuery {
    name: String,
    config: Config,
}
struct Config {
    units: Units,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct GQLResponse {
    data: Data,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct Data {
    #[serde(rename = "getCityByName")]
    get_city_by_name: GetCityByNameResponse,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct GetCityByNameResponse {
    name: String,
    country: String,
    weather: Weatherq,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct Summary {
    title: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct Weatherq {
    summary: Summary,
    temperature: Temperature,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct Temperature {
    actual: f32,
    #[serde(rename = "feelsLike")]
    feels_like: f32,
    min: f32,
    max: f32,
}
