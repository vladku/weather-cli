use crate::{Units, WeatherInfo, WeatherProvider};

pub struct OpenWeatherProvider {
    api_key: String,
    loc_url: String,
    weather_url: String,
}

impl OpenWeatherProvider {
    pub fn new(api_key: String) -> Self {
        let url = "http://api.openweathermap.org";
        OpenWeatherProvider {
            api_key,
            loc_url: format!("{}/geo/1.0/direct", url),
            weather_url: format!("{}/data/2.5/weather", url),
        }
    }
    fn get_location(&self, city: &str) -> Result<Location, String> {
        let path: String = format!("{}?q={}&limit=1&appid={}", self.loc_url, city, self.api_key);
        match reqwest::blocking::get(&path) {
            Ok(r) => match r.json::<Vec<Location>>() {
                Ok(r) => Ok(r[0].clone()),
                Err(e) => Err(e.to_string()),
            },
            Err(e) => Err(e.to_string()),
        }
    }
    fn get_weather(&self, loc: Location, units: Units) -> Result<Weather, String> {
        let path: String = format!(
            "{}?lat={}&lon={}&appid={}&units={}",
            self.weather_url, loc.lat, loc.lon, self.api_key, units
        );
        match reqwest::blocking::get(&path) {
            Ok(r) => match r.json::<Weather>() {
                Ok(r) => Ok(r),
                Err(e) => Err(e.to_string()),
            },
            Err(e) => Err(e.to_string()),
        }
    }
}
impl WeatherProvider for OpenWeatherProvider {
    fn get(&self, city: &str, units: Units) -> Result<WeatherInfo, String> {
        let loc = match self.get_location(city) {
            Ok(loc) => loc,
            Err(e) => {
                return Err(format!(
                    "Failed to get location by city name {}. {}",
                    city, e
                ))
            }
        };
        let info = match self.get_weather(loc, units) {
            Ok(loc) => loc,
            Err(e) => return Err(e),
        };

        Ok(WeatherInfo {
            summary: info.weather[0].main.clone(),
            temperature: info.main.temp,
            feels_like: info.main.feels_like,
            min: info.main.temp_min,
            max: info.main.temp_max,
            city: info.name,
            country: info.sys.country,
            units,
        })
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Location {
    name: String,
    lat: f32,
    lon: f32,
    country: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct Weather {
    weather: Vec<WeatherMainInfo>,
    main: MainWeather,
    sys: WeatherSys,
    name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct WeatherSys {
    country: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct WeatherMainInfo {
    main: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
struct MainWeather {
    temp: f32,
    feels_like: f32,
    temp_min: f32,
    temp_max: f32,
}
